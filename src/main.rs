use std::{fs::{self, File}, io::Error, path::Path};
use sha2::{Sha256, Digest};
use std::collections::HashMap;

/*
 * Iterations:
 *
 * 1. Walk and read files in testdata/ ✅
 * 2. Calculate the sha256sum ✅
 * 3. Group files by sha256sum hexstring
 * 4. Optimize use output directly as key
 *
 */
fn main() {
    let dir = Path::new("./testdata");
    walk_directory(dir);
}

fn walk_directory(dir: &Path) -> Result<(), Error> {
    let duplicates_by_sum: HashMap<String, &mut Vec<&Path>> = HashMap::new();

    for entry in fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();

        if path.is_dir() {
            walk_directory(&path);
        } else {
            let content = fs::read(&path)?;
            let mut hasher = Sha256::new();
            hasher.update(content);
            let bytes = hasher.finalize();
            let k = format!("{:x}", bytes);
            match duplicates_by_sum.get(&k) {
                Some(duplicates) => {
                    duplicates.push(&path);
                },
                None => {
                    println!("none")
                }
            }
        }
    }

    Ok(())
}
